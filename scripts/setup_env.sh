case ${CI_COMMIT_REF_NAME} in
    "dev")
        export ENVIRONMENT="dev"
        export HEROKU_APP="flask-restful-demo-dev"
        ;;
    "qas")
        export ENVIRONMENT="qas"
        export HEROKU_APP="flask-restful-demo-qas"
        ;;
esac
echo "Enviroment OK '${HEROKU_APP}'"